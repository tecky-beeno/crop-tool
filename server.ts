import fs from 'fs';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import rimraf from 'rimraf'

let app = express();
app.use(bodyParser.json({ limit: '1mb' }));

let rawDir = path.join('res', 'raw');
let croppedDir = path.join('res', 'cropped');

fs.mkdirSync(croppedDir, { recursive: true });

function getNextImage(): string | null {
  let labels = fs.readdirSync(rawDir);
  for (let label of labels) {
    let labelDir = path.join(rawDir, label);
    for (let image of fs.readdirSync(labelDir)) {
      let file = path.join(labelDir, image);
      return file;
    }
  }
  return null;
}

function dataUrlToBuffer(dataUrl: string) {
  let idx = dataUrl.indexOf(',');
  let base64 = dataUrl.substr(idx + 1);
  return Buffer.from(base64, 'base64');
}

async function saveImage(src: string, dataUrl: string) {
  let parts = src.split('/');
  parts.pop();
  let label = parts.pop();
  // res/cropped/char-1.jpg
  let destFile = path.join(croppedDir, label) + '.jpg';
  let srcDir = path.join(rawDir, label);
  console.log('save to', destFile);

  let buffer = dataUrlToBuffer(dataUrl);
  fs.writeFileSync(destFile, buffer);

  console.log('del', srcDir);
  rimraf.sync(srcDir)
}

app.get('/next-image', (req, res) => {
  let file = getNextImage();
  res.send(file);
});

app.post('/save-image', async (req, res) => {
  let { dataUrl, src } = req.body;
  console.log(src, dataUrl.length);
  await saveImage(src, dataUrl);
  res.send('ok')
});

app.use('/res', express.static('res'));
app.use(express.static('public'));

let port = 8100;
app.listen(port, () => {
  console.log('listening on http://localhost:' + port);
});
